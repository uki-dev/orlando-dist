-- MySQL dump 10.13  Distrib 5.7.33, for Linux (x86_64)
--
-- Host: 70.164.1.222    Database: orlando_db
-- ------------------------------------------------------
-- Server version	5.7.27-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `assets`
--

DROP TABLE IF EXISTS `assets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `model` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `manufacturer` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `serialNumber` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `operatingSystem` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hardDriveSize` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ramAmount` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `userId` int(10) unsigned DEFAULT NULL,
  `organizationId` int(10) unsigned DEFAULT NULL,
  `locationId` int(10) unsigned DEFAULT NULL,
  `classroomId` int(10) unsigned DEFAULT NULL,
  `notes` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `assets_userid_foreign` (`userId`),
  KEY `assets_organizationid_foreign` (`organizationId`),
  KEY `assets_locationid_fk` (`locationId`),
  KEY `assets_classroomid_fk` (`classroomId`),
  CONSTRAINT `assets_classroomid_fk` FOREIGN KEY (`classroomId`) REFERENCES `classrooms` (`id`),
  CONSTRAINT `assets_locationid_fk` FOREIGN KEY (`locationId`) REFERENCES `locations` (`id`),
  CONSTRAINT `assets_organizationid_foreign` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`),
  CONSTRAINT `assets_userid_foreign` FOREIGN KEY (`userId`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `assignmentCommentTopics`
--

DROP TABLE IF EXISTS `assignmentCommentTopics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assignmentCommentTopics` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `assignmentId` int(10) unsigned NOT NULL,
  `courseCollaboratorId` int(10) unsigned NOT NULL,
  `isClosed` tinyint(1) NOT NULL DEFAULT '0',
  `isRemoved` tinyint(1) NOT NULL DEFAULT '0',
  `xPos` tinyint(3) unsigned NOT NULL,
  `yPos` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `assignmentcommenttopics_assignmentid_fk` (`assignmentId`),
  KEY `assignmentcommenttopics_coursecollaboratorid_fk` (`courseCollaboratorId`),
  CONSTRAINT `assignmentcommenttopics_assignmentid_fk` FOREIGN KEY (`assignmentId`) REFERENCES `assignments` (`id`),
  CONSTRAINT `assignmentcommenttopics_coursecollaboratorid_fk` FOREIGN KEY (`courseCollaboratorId`) REFERENCES `courseCollaborators` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `assignmentComments`
--

DROP TABLE IF EXISTS `assignmentComments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assignmentComments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content` varchar(1000) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `courseCollaboratorId` int(10) unsigned NOT NULL,
  `assignmentCommentTopicId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `assignmentcomments_coursecollaboratorid_fk` (`courseCollaboratorId`),
  KEY `assignmentcomments_assignmentcommenttopicid_fk` (`assignmentCommentTopicId`),
  CONSTRAINT `assignmentcomments_assignmentcommenttopicid_fk` FOREIGN KEY (`assignmentCommentTopicId`) REFERENCES `assignmentCommentTopics` (`id`),
  CONSTRAINT `assignmentcomments_coursecollaboratorid_fk` FOREIGN KEY (`courseCollaboratorId`) REFERENCES `courseCollaborators` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `assignmentLayouts`
--

DROP TABLE IF EXISTS `assignmentLayouts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assignmentLayouts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `layout` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `assignmentId` int(10) unsigned NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `assignmentlayouts_assignmentid_foreign` (`assignmentId`),
  KEY `assignmentlayouts_organizationid_foreign` (`organizationId`),
  CONSTRAINT `assignmentlayouts_assignmentid_foreign` FOREIGN KEY (`assignmentId`) REFERENCES `assignments` (`id`),
  CONSTRAINT `assignmentlayouts_organizationid_foreign` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `assignments`
--

DROP TABLE IF EXISTS `assignments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assignments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `moduleId` int(10) unsigned NOT NULL,
  `courseId` int(10) unsigned NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `courseELOId` int(10) unsigned DEFAULT NULL,
  `stackId` int(10) unsigned DEFAULT NULL,
  `courseExamId` int(10) unsigned DEFAULT NULL,
  `labId` int(10) unsigned DEFAULT NULL,
  `scormPackageId` int(10) unsigned DEFAULT NULL,
  `forGrade` tinyint(1) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `organizationId` int(10) unsigned NOT NULL,
  `courseTLOId` int(10) unsigned DEFAULT NULL,
  `moduleSectionId` int(10) unsigned DEFAULT NULL,
  `courseVideoId` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `assignments_moduleid_foreign` (`moduleId`),
  KEY `assignments_courseid_foreign` (`courseId`),
  KEY `assignments_courseeloid_foreign` (`courseELOId`),
  KEY `assignments_stackid_foreign` (`stackId`),
  KEY `assignments_courseexamid_foreign` (`courseExamId`),
  KEY `assignments_labid_foreign` (`labId`),
  KEY `assignments_scormpackageid_foreign` (`scormPackageId`),
  KEY `assignments_organizationid_foreign` (`organizationId`),
  KEY `assignments_coursetloid_fk` (`courseTLOId`),
  KEY `assignments_modulesectionid_fk` (`moduleSectionId`),
  KEY `assignments_coursevideoid_fk` (`courseVideoId`),
  CONSTRAINT `assignments_courseeloid_foreign` FOREIGN KEY (`courseELOId`) REFERENCES `courseELOs` (`id`),
  CONSTRAINT `assignments_courseexamid_foreign` FOREIGN KEY (`courseExamId`) REFERENCES `courseExams` (`id`),
  CONSTRAINT `assignments_courseid_foreign` FOREIGN KEY (`courseId`) REFERENCES `courses` (`id`),
  CONSTRAINT `assignments_coursetloid_fk` FOREIGN KEY (`courseTLOId`) REFERENCES `courseTLOs` (`id`),
  CONSTRAINT `assignments_coursevideoid_fk` FOREIGN KEY (`courseVideoId`) REFERENCES `courseVideos` (`id`),
  CONSTRAINT `assignments_labid_foreign` FOREIGN KEY (`labId`) REFERENCES `labs` (`id`),
  CONSTRAINT `assignments_moduleid_foreign` FOREIGN KEY (`moduleId`) REFERENCES `modules` (`id`),
  CONSTRAINT `assignments_modulesectionid_fk` FOREIGN KEY (`moduleSectionId`) REFERENCES `moduleSections` (`id`),
  CONSTRAINT `assignments_organizationid_foreign` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`),
  CONSTRAINT `assignments_scormpackageid_foreign` FOREIGN KEY (`scormPackageId`) REFERENCES `scormPackages` (`id`),
  CONSTRAINT `assignments_stackid_foreign` FOREIGN KEY (`stackId`) REFERENCES `stacks` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `calendarEvents`
--

DROP TABLE IF EXISTS `calendarEvents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calendarEvents` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `userId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `calendarevents_userid_fk` (`userId`),
  CONSTRAINT `calendarevents_userid_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cards`
--

DROP TABLE IF EXISTS `cards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cards` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `front` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `back` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `stackId` int(10) unsigned NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cards_stackid_foreign` (`stackId`),
  KEY `cards_organizationid_foreign` (`organizationId`),
  CONSTRAINT `cards_organizationid_foreign` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`),
  CONSTRAINT `cards_stackid_foreign` FOREIGN KEY (`stackId`) REFERENCES `stacks` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `certifications`
--

DROP TABLE IF EXISTS `certifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `certifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ceus` smallint(5) unsigned DEFAULT NULL,
  `certLength` tinyint(3) unsigned DEFAULT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `vendorId` int(10) unsigned NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  `image` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `certifications_vendorid_foreign` (`vendorId`),
  KEY `certifications_organizationid_foreign` (`organizationId`),
  CONSTRAINT `certifications_organizationid_foreign` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`),
  CONSTRAINT `certifications_vendorid_foreign` FOREIGN KEY (`vendorId`) REFERENCES `vendors` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ceuDocs`
--

DROP TABLE IF EXISTS `ceuDocs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ceuDocs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `docTitle` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `ceuId` int(10) unsigned NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ceudocs_ceuid_foreign` (`ceuId`),
  KEY `ceudocs_organizationid_foreign` (`organizationId`),
  CONSTRAINT `ceudocs_ceuid_foreign` FOREIGN KEY (`ceuId`) REFERENCES `ceus` (`id`),
  CONSTRAINT `ceudocs_organizationid_foreign` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ceus`
--

DROP TABLE IF EXISTS `ceus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ceus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activityType` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hours` tinyint(3) unsigned NOT NULL,
  `dateAchieved` date NOT NULL,
  `vendorConfirmed` tinyint(1) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `urlToActivity` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `userId` int(10) unsigned NOT NULL,
  `dateSubmitted` date DEFAULT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ceus_userid_foreign` (`userId`),
  KEY `ceus_organizationid_foreign` (`organizationId`),
  CONSTRAINT `ceus_organizationid_foreign` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`),
  CONSTRAINT `ceus_userid_foreign` FOREIGN KEY (`userId`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ceusToCertifications`
--

DROP TABLE IF EXISTS `ceusToCertifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ceusToCertifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ceuId` int(10) unsigned NOT NULL,
  `userCertificationId` int(10) unsigned NOT NULL,
  `dateCreated` datetime NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ceustocertifications_usercertificationid_foreign` (`userCertificationId`),
  KEY `ceustocertifications_ceuid_foreign` (`ceuId`),
  KEY `ceustocertifications_organizationid_foreign` (`organizationId`),
  CONSTRAINT `ceustocertifications_ceuid_foreign` FOREIGN KEY (`ceuId`) REFERENCES `ceus` (`id`),
  CONSTRAINT `ceustocertifications_organizationid_foreign` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`),
  CONSTRAINT `ceustocertifications_usercertificationid_foreign` FOREIGN KEY (`userCertificationId`) REFERENCES `userCertifications` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `classDocs`
--

DROP TABLE IF EXISTS `classDocs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `classDocs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `classId` int(10) unsigned NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  `description` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fileSize` float unsigned DEFAULT NULL,
  `mime` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `classdocs_classid_foreign` (`classId`),
  KEY `classdocs_organizationid_foreign` (`organizationId`),
  CONSTRAINT `classdocs_classid_foreign` FOREIGN KEY (`classId`) REFERENCES `classes` (`id`),
  CONSTRAINT `classdocs_organizationid_foreign` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `classes`
--

DROP TABLE IF EXISTS `classes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `classes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alerts` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `startTimeZone` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `startDate` date DEFAULT NULL,
  `startTime` time DEFAULT NULL,
  `endTimeZone` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `endDate` date DEFAULT NULL,
  `endTime` time DEFAULT NULL,
  `locationAccessInfo` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `haveMaterialsArrived` tinyint(1) NOT NULL DEFAULT '0',
  `dateMaterialsArrived` date DEFAULT NULL,
  `materialsTrackingInfo` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `laptopPowerSupplies` smallint(5) unsigned DEFAULT NULL,
  `switchPowerSupplies` smallint(5) unsigned DEFAULT NULL,
  `printerPowerSupplies` smallint(5) unsigned DEFAULT NULL,
  `extensionCords` smallint(5) unsigned DEFAULT NULL,
  `cat5Cables` smallint(5) unsigned DEFAULT NULL,
  `surgeProtectors` smallint(5) unsigned DEFAULT NULL,
  `equipmentUsername` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `equipmentPassword` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `equipmentOperatingSystem` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `equipmentSoftware` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `equipmentISOs` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `equipmentVMs` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `locationId` int(10) unsigned DEFAULT NULL,
  `coursewareId` int(10) unsigned DEFAULT NULL,
  `instructorId` int(10) unsigned DEFAULT NULL,
  `courseId` int(10) unsigned NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  `dateCreated` datetime NOT NULL,
  `isInPerson` tinyint(1) NOT NULL DEFAULT '0',
  `isOnline` tinyint(1) NOT NULL DEFAULT '0',
  `isSelfPaced` tinyint(1) NOT NULL DEFAULT '0',
  `registration` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `minSeats` smallint(5) unsigned DEFAULT NULL,
  `maxSeats` smallint(5) unsigned DEFAULT NULL,
  `isPublic` tinyint(1) NOT NULL DEFAULT '0',
  `isInstructorConfirmed` tinyint(1) NOT NULL DEFAULT '0',
  `classroomId` int(10) unsigned DEFAULT NULL,
  `seatPrice` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `refundExpiryDate` date DEFAULT NULL,
  `voucherCost` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `studentVoucherPrice` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `printMaterialsCost` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `printMaterialsQty` smallint(5) unsigned NOT NULL DEFAULT '0',
  `bookCost` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `bookQty` smallint(5) unsigned NOT NULL DEFAULT '0',
  `instructorRate` decimal(10,2) NOT NULL DEFAULT '0.00',
  `isInstructorRateDaily` tinyint(1) NOT NULL DEFAULT '0',
  `instructorPerDiemRate` decimal(10,2) NOT NULL DEFAULT '0.00',
  `voucherQty` smallint(5) unsigned NOT NULL DEFAULT '0',
  `isVoucherIncluded` tinyint(1) NOT NULL DEFAULT '0',
  `paymentAccountingCode` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instructorDepartureDate` date DEFAULT NULL,
  `instructorDepartureTime` time DEFAULT NULL,
  `instructorDepartureAirport` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instructorDepartureAirline` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instructorDepartureFlightNumber` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instructorDepartureArrivalDate` date DEFAULT NULL,
  `instructorDepartureArrivalTime` time DEFAULT NULL,
  `instructorReturnDate` date DEFAULT NULL,
  `instructorReturnTime` time DEFAULT NULL,
  `instructorReturnAirport` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instructorReturnAirline` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instructorReturnFlightNumber` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instructorReturnArrivalDate` date DEFAULT NULL,
  `instructorReturnArrivalTime` time DEFAULT NULL,
  `instructorHotelConfirmationNumber` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instructorHotelPhone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instructorHotelAddress` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instructorHotelCity` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instructorHotelState` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instructorHotelZip` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instructorCarConfirmationNumber` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instructorCarCompany` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instructorCarPhone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instructorFlightCost` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `instructorHotelCost` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `instructorCarCost` decimal(10,2) unsigned NOT NULL DEFAULT '0.00',
  `quoteId` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quoteNumber` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isCrmQuote` tinyint(1) NOT NULL DEFAULT '0',
  `contactId` int(10) unsigned DEFAULT NULL,
  `isVoucherApplicable` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `classes_locationid_foreign` (`locationId`),
  KEY `classes_coursewareid_foreign` (`coursewareId`),
  KEY `classes_instructorid_foreign` (`instructorId`),
  KEY `classes_courseid_foreign` (`courseId`),
  KEY `classes_organizationid_foreign` (`organizationId`),
  KEY `classes_classroomid_fk` (`classroomId`),
  KEY `classes_contactid_fk` (`contactId`),
  CONSTRAINT `classes_classroomid_fk` FOREIGN KEY (`classroomId`) REFERENCES `classrooms` (`id`),
  CONSTRAINT `classes_contactid_fk` FOREIGN KEY (`contactId`) REFERENCES `users` (`id`),
  CONSTRAINT `classes_courseid_foreign` FOREIGN KEY (`courseId`) REFERENCES `courses` (`id`),
  CONSTRAINT `classes_coursewareid_foreign` FOREIGN KEY (`coursewareId`) REFERENCES `courseware` (`id`),
  CONSTRAINT `classes_instructorid_foreign` FOREIGN KEY (`instructorId`) REFERENCES `users` (`id`),
  CONSTRAINT `classes_locationid_foreign` FOREIGN KEY (`locationId`) REFERENCES `locations` (`id`),
  CONSTRAINT `classes_organizationid_foreign` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `classrooms`
--

DROP TABLE IF EXISTS `classrooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `classrooms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `capacity` mediumint(8) unsigned DEFAULT NULL,
  `internetAccess` tinyint(1) DEFAULT NULL,
  `locationId` int(10) unsigned NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  `dateCreated` datetime NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `classrooms_locationid_fk` (`locationId`),
  KEY `classrooms_organizationid_fk` (`organizationId`),
  CONSTRAINT `classrooms_locationid_fk` FOREIGN KEY (`locationId`) REFERENCES `locations` (`id`),
  CONSTRAINT `classrooms_organizationid_fk` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `courseAccesses`
--

DROP TABLE IF EXISTS `courseAccesses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courseAccesses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `courseId` int(10) unsigned NOT NULL,
  `userId` int(10) unsigned NOT NULL,
  `dateAccessed` datetime NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `courseaccesses_courseid_fk` (`courseId`),
  KEY `courseaccesses_userid_fk` (`userId`),
  KEY `courseaccesses_organizationid_fk` (`organizationId`),
  CONSTRAINT `courseaccesses_courseid_fk` FOREIGN KEY (`courseId`) REFERENCES `courses` (`id`),
  CONSTRAINT `courseaccesses_organizationid_fk` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`),
  CONSTRAINT `courseaccesses_userid_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `courseCartItems`
--

DROP TABLE IF EXISTS `courseCartItems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courseCartItems` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `classId` int(10) unsigned NOT NULL,
  `userId` int(10) unsigned NOT NULL,
  `dateCreated` datetime NOT NULL,
  `isTesting` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `coursecart_classid_fk` (`classId`),
  KEY `coursecart_userid_fk` (`userId`),
  CONSTRAINT `coursecart_classid_fk` FOREIGN KEY (`classId`) REFERENCES `classes` (`id`),
  CONSTRAINT `coursecart_userid_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `courseCollaborators`
--

DROP TABLE IF EXISTS `courseCollaborators`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courseCollaborators` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(10) unsigned NOT NULL,
  `courseId` int(10) unsigned NOT NULL,
  `dateCreated` datetime NOT NULL,
  `color` varchar(50) NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `coursecollaborators_userid_fk` (`userId`),
  KEY `coursecollaborators_courseid_fk` (`courseId`),
  CONSTRAINT `coursecollaborators_courseid_fk` FOREIGN KEY (`courseId`) REFERENCES `courses` (`id`),
  CONSTRAINT `coursecollaborators_userid_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `courseCommentTopics`
--

DROP TABLE IF EXISTS `courseCommentTopics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courseCommentTopics` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `courseId` int(10) unsigned NOT NULL,
  `courseCollaboratorId` int(10) unsigned NOT NULL,
  `isClosed` tinyint(1) NOT NULL DEFAULT '0',
  `isRemoved` tinyint(1) NOT NULL DEFAULT '0',
  `xPos` tinyint(3) unsigned NOT NULL,
  `yPos` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `coursecommenttopics_courseid_fk` (`courseId`),
  KEY `coursecommenttopics_coursecollaboratorid_fk` (`courseCollaboratorId`),
  CONSTRAINT `coursecommenttopics_coursecollaboratorid_fk` FOREIGN KEY (`courseCollaboratorId`) REFERENCES `courseCollaborators` (`id`),
  CONSTRAINT `coursecommenttopics_courseid_fk` FOREIGN KEY (`courseId`) REFERENCES `courses` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `courseComments`
--

DROP TABLE IF EXISTS `courseComments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courseComments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content` varchar(1000) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `courseCollaboratorId` int(10) unsigned NOT NULL,
  `courseCommentTopicId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `coursecomments_coursecollaboratorid_fk` (`courseCollaboratorId`),
  KEY `coursecomments_coursecommenttopicid_fk` (`courseCommentTopicId`),
  CONSTRAINT `coursecomments_coursecollaboratorid_fk` FOREIGN KEY (`courseCollaboratorId`) REFERENCES `courseCollaborators` (`id`),
  CONSTRAINT `coursecomments_coursecommenttopicid_fk` FOREIGN KEY (`courseCommentTopicId`) REFERENCES `courseCommentTopics` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `courseDocs`
--

DROP TABLE IF EXISTS `courseDocs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courseDocs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `courseId` int(10) unsigned NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  `fileSize` float unsigned DEFAULT NULL,
  `mime` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `coursedocs_courseid_foreign` (`courseId`),
  KEY `coursedocs_organizationid_foreign` (`organizationId`),
  CONSTRAINT `coursedocs_courseid_foreign` FOREIGN KEY (`courseId`) REFERENCES `courses` (`id`),
  CONSTRAINT `coursedocs_organizationid_foreign` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `courseELOs`
--

DROP TABLE IF EXISTS `courseELOs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courseELOs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `courseTLOId` int(10) unsigned NOT NULL,
  `courseId` int(10) unsigned NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `courseelos_coursetloid_foreign` (`courseTLOId`),
  KEY `courseelos_courseid_foreign` (`courseId`),
  KEY `courseelos_organizationid_foreign` (`organizationId`),
  CONSTRAINT `courseelos_courseid_foreign` FOREIGN KEY (`courseId`) REFERENCES `courses` (`id`),
  CONSTRAINT `courseelos_coursetloid_foreign` FOREIGN KEY (`courseTLOId`) REFERENCES `courseTLOs` (`id`),
  CONSTRAINT `courseelos_organizationid_foreign` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `courseExamAnswers`
--

DROP TABLE IF EXISTS `courseExamAnswers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courseExamAnswers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `answerText` text COLLATE utf8mb4_unicode_ci,
  `isCorrect` tinyint(1) DEFAULT NULL,
  `sortOrder` tinyint(3) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `courseExamQuestionId` int(10) unsigned NOT NULL,
  `courseExamId` int(10) unsigned NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  `matchText` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `courseexamanswers_courseexamquestionid_foreign` (`courseExamQuestionId`),
  KEY `courseexamanswers_courseexamid_foreign` (`courseExamId`),
  KEY `courseexamanswers_organizationid_foreign` (`organizationId`),
  CONSTRAINT `courseexamanswers_courseexamid_foreign` FOREIGN KEY (`courseExamId`) REFERENCES `courseExams` (`id`),
  CONSTRAINT `courseexamanswers_courseexamquestionid_foreign` FOREIGN KEY (`courseExamQuestionId`) REFERENCES `courseExamQuestions` (`id`),
  CONSTRAINT `courseexamanswers_organizationid_foreign` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `courseExamQuestions`
--

DROP TABLE IF EXISTS `courseExamQuestions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courseExamQuestions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `questionText` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `explanation` text COLLATE utf8mb4_unicode_ci,
  `courseExamId` int(10) unsigned NOT NULL,
  `dateCreated` datetime NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `organizationId` int(10) unsigned NOT NULL,
  `answerText` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentQuestionId` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `courseexamquestions_courseexamid_foreign` (`courseExamId`),
  KEY `courseexamquestions_organizationid_foreign` (`organizationId`),
  KEY `courseExamQuestions_parentQuestionId_FK` (`parentQuestionId`),
  CONSTRAINT `courseExamQuestions_parentQuestionId_FK` FOREIGN KEY (`parentQuestionId`) REFERENCES `courseExamQuestions` (`id`),
  CONSTRAINT `courseexamquestions_courseexamid_foreign` FOREIGN KEY (`courseExamId`) REFERENCES `courseExams` (`id`),
  CONSTRAINT `courseexamquestions_organizationid_foreign` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `courseExams`
--

DROP TABLE IF EXISTS `courseExams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courseExams` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `courseId` int(10) unsigned DEFAULT NULL,
  `timeLimit` smallint(5) unsigned DEFAULT NULL,
  `minPassingScore` tinyint(3) unsigned NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  `type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `experience` text COLLATE utf8mb4_unicode_ci,
  `target` text COLLATE utf8mb4_unicode_ci,
  `introduction` text COLLATE utf8mb4_unicode_ci,
  `questionCount` mediumint(8) unsigned DEFAULT NULL,
  `weight` decimal(5,2) unsigned NOT NULL DEFAULT '0.00',
  `isActive` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `courseexams_courseid_foreign` (`courseId`),
  KEY `courseexams_organizationid_foreign` (`organizationId`),
  CONSTRAINT `courseexams_courseid_foreign` FOREIGN KEY (`courseId`) REFERENCES `courses` (`id`),
  CONSTRAINT `courseexams_organizationid_foreign` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `courseImages`
--

DROP TABLE IF EXISTS `courseImages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courseImages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `isFile` tinyint(1) NOT NULL DEFAULT '0',
  `link` varchar(2048) DEFAULT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `courseId` int(10) unsigned DEFAULT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  `dateCreated` datetime NOT NULL,
  `fileLink` varchar(2048) DEFAULT NULL,
  `fileSize` float unsigned DEFAULT NULL,
  `parentId` int(10) unsigned DEFAULT NULL,
  `isAuthoritative` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `courseimages_courseid_fk` (`courseId`),
  KEY `courseimages_organizationid_fk` (`organizationId`),
  KEY `courseimages_parentid_fk` (`parentId`),
  CONSTRAINT `courseimages_courseid_fk` FOREIGN KEY (`courseId`) REFERENCES `courses` (`id`),
  CONSTRAINT `courseimages_organizationid_fk` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`),
  CONSTRAINT `courseimages_parentid_fk` FOREIGN KEY (`parentId`) REFERENCES `courseImages` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `courseLayouts`
--

DROP TABLE IF EXISTS `courseLayouts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courseLayouts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `layout` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `courseId` int(10) unsigned NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `courselayouts_courseid_foreign` (`courseId`),
  KEY `courselayouts_organizationid_foreign` (`organizationId`),
  CONSTRAINT `courselayouts_courseid_foreign` FOREIGN KEY (`courseId`) REFERENCES `courses` (`id`),
  CONSTRAINT `courselayouts_organizationid_foreign` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `coursePrereqs`
--

DROP TABLE IF EXISTS `coursePrereqs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coursePrereqs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `courseId` int(10) unsigned NOT NULL,
  `prereqCourseId` int(10) unsigned NOT NULL,
  `dateCreated` datetime NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `courseprereqs_courseid_fk` (`courseId`),
  KEY `courseprereqs_prereqcourseid_fk` (`prereqCourseId`),
  KEY `courseprereqs_organizationid_fk` (`organizationId`),
  CONSTRAINT `courseprereqs_courseid_fk` FOREIGN KEY (`courseId`) REFERENCES `courses` (`id`),
  CONSTRAINT `courseprereqs_organizationid_fk` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`),
  CONSTRAINT `courseprereqs_prereqcourseid_fk` FOREIGN KEY (`prereqCourseId`) REFERENCES `courses` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `courseTLOs`
--

DROP TABLE IF EXISTS `courseTLOs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courseTLOs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `dateCreated` datetime NOT NULL,
  `moduleId` int(10) unsigned NOT NULL,
  `courseId` int(10) unsigned NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `organizationId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `coursetlos_moduleid_foreign` (`moduleId`),
  KEY `coursetlos_courseid_foreign` (`courseId`),
  KEY `courseTLOs_organizationid_foreign` (`organizationId`),
  CONSTRAINT `courseTLOs_organizationid_foreign` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`),
  CONSTRAINT `coursetlos_courseid_foreign` FOREIGN KEY (`courseId`) REFERENCES `courses` (`id`),
  CONSTRAINT `coursetlos_moduleid_foreign` FOREIGN KEY (`moduleId`) REFERENCES `modules` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `courseVideos`
--

DROP TABLE IF EXISTS `courseVideos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courseVideos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `isFile` tinyint(1) NOT NULL DEFAULT '0',
  `link` varchar(2048) DEFAULT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `courseId` int(10) unsigned DEFAULT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  `dateCreated` datetime NOT NULL,
  `fileLink` varchar(2048) DEFAULT NULL,
  `fileSize` float unsigned DEFAULT NULL,
  `parentId` int(10) unsigned DEFAULT NULL,
  `isAuthoritative` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `coursevideos_courseid_fk` (`courseId`),
  KEY `coursevideos_organizationid_foreign` (`organizationId`),
  KEY `coursevideos_parentid_fk` (`parentId`),
  CONSTRAINT `coursevideos_courseid_fk` FOREIGN KEY (`courseId`) REFERENCES `courses` (`id`),
  CONSTRAINT `coursevideos_organizationid_foreign` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`),
  CONSTRAINT `coursevideos_parentid_fk` FOREIGN KEY (`parentId`) REFERENCES `courseVideos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `courseWebLinks`
--

DROP TABLE IF EXISTS `courseWebLinks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courseWebLinks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isFile` tinyint(1) NOT NULL,
  `fileLink` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `courseId` int(10) unsigned NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  `fileSize` float unsigned DEFAULT NULL,
  `mime` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `courseweblinks_courseid_foreign` (`courseId`),
  KEY `courseweblinks_organizationid_foreign` (`organizationId`),
  CONSTRAINT `courseweblinks_courseid_foreign` FOREIGN KEY (`courseId`) REFERENCES `courses` (`id`),
  CONSTRAINT `courseweblinks_organizationid_foreign` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `courses`
--

DROP TABLE IF EXISTS `courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abbreviation` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `length` int(3) unsigned DEFAULT NULL,
  `outline` text COLLATE utf8mb4_unicode_ci,
  `outlineFile` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `minPassingScore` tinyint(3) unsigned NOT NULL,
  `certification` text COLLATE utf8mb4_unicode_ci,
  `whatYouGet` text COLLATE utf8mb4_unicode_ci,
  `whatYouLearn` text COLLATE utf8mb4_unicode_ci,
  `prereqs` text COLLATE utf8mb4_unicode_ci,
  `idealCandidate` text COLLATE utf8mb4_unicode_ci,
  `equipmentConfig` text COLLATE utf8mb4_unicode_ci,
  `audience` text COLLATE utf8mb4_unicode_ci,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `isPublic` tinyint(1) NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `vendorId` int(10) unsigned DEFAULT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  `isArchived` tinyint(1) NOT NULL DEFAULT '0',
  `isInPerson` tinyint(1) NOT NULL DEFAULT '0',
  `isOnline` tinyint(1) NOT NULL DEFAULT '0',
  `isSelfPaced` tinyint(1) NOT NULL DEFAULT '0',
  `certificationId` int(10) unsigned DEFAULT NULL,
  `requirements` text COLLATE utf8mb4_unicode_ci,
  `structure` text COLLATE utf8mb4_unicode_ci,
  `instructorActive` tinyint(1) NOT NULL DEFAULT '1',
  `prereqsActive` tinyint(1) NOT NULL DEFAULT '1',
  `descriptionActive` tinyint(1) NOT NULL DEFAULT '1',
  `certificationActive` tinyint(1) NOT NULL DEFAULT '1',
  `coursewareActive` tinyint(1) NOT NULL DEFAULT '1',
  `requirementsActive` tinyint(1) NOT NULL DEFAULT '1',
  `structureActive` tinyint(1) NOT NULL DEFAULT '1',
  `learningOutcomes` text COLLATE utf8mb4_unicode_ci,
  `roadsToSuccess` text COLLATE utf8mb4_unicode_ci,
  `learningOutcomesActive` tinyint(1) NOT NULL DEFAULT '1',
  `roadsToSuccessActive` tinyint(1) NOT NULL DEFAULT '1',
  `outlineActive` tinyint(1) NOT NULL DEFAULT '1',
  `lateWork` text COLLATE utf8mb4_unicode_ci,
  `viewingGrades` text COLLATE utf8mb4_unicode_ci,
  `lateWorkActive` tinyint(1) NOT NULL DEFAULT '1',
  `viewingGradesActive` tinyint(1) NOT NULL DEFAULT '1',
  `letterGradesActive` tinyint(1) NOT NULL DEFAULT '1',
  `attendClass` text COLLATE utf8mb4_unicode_ci,
  `participate` text COLLATE utf8mb4_unicode_ci,
  `buildRapport` text COLLATE utf8mb4_unicode_ci,
  `completeAssignments` text COLLATE utf8mb4_unicode_ci,
  `droppingCourse` text COLLATE utf8mb4_unicode_ci,
  `incompletePolicy` text COLLATE utf8mb4_unicode_ci,
  `accommodations` text COLLATE utf8mb4_unicode_ci,
  `integrity` text COLLATE utf8mb4_unicode_ci,
  `academicDishonesty` text COLLATE utf8mb4_unicode_ci,
  `attendClassActive` tinyint(1) NOT NULL DEFAULT '1',
  `participateActive` tinyint(1) NOT NULL DEFAULT '1',
  `buildRapportActive` tinyint(1) NOT NULL DEFAULT '1',
  `completeAssignmentsActive` tinyint(1) NOT NULL DEFAULT '1',
  `droppingCourseActive` tinyint(1) NOT NULL DEFAULT '1',
  `incompletePolicyActive` tinyint(1) NOT NULL DEFAULT '1',
  `accommodationsActive` tinyint(1) NOT NULL DEFAULT '1',
  `integrityActive` tinyint(1) NOT NULL DEFAULT '1',
  `academicDishonestyActive` tinyint(1) NOT NULL DEFAULT '1',
  `letterGrades` text COLLATE utf8mb4_unicode_ci,
  `isPopular` tinyint(1) NOT NULL DEFAULT '0',
  `shortDescription` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enableSyllabus` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `courses_vendorid_foreign` (`vendorId`),
  KEY `courses_organizationid_foreign` (`organizationId`),
  KEY `courses_certificationid_fk` (`certificationId`),
  CONSTRAINT `courses_certificationid_fk` FOREIGN KEY (`certificationId`) REFERENCES `certifications` (`id`),
  CONSTRAINT `courses_organizationid_foreign` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`),
  CONSTRAINT `courses_vendorid_foreign` FOREIGN KEY (`vendorId`) REFERENCES `vendors` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `courseware`
--

DROP TABLE IF EXISTS `courseware`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courseware` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isbn` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cost` decimal(10,2) unsigned NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `courseId` int(10) unsigned DEFAULT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  `pdf` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fileSize` float unsigned DEFAULT NULL,
  `parentId` int(10) unsigned DEFAULT NULL,
  `isAuthoritative` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `courseware_courseid_foreign` (`courseId`),
  KEY `courseware_organizationid_foreign` (`organizationId`),
  KEY `courseware_parentid_fk` (`parentId`),
  CONSTRAINT `courseware_courseid_foreign` FOREIGN KEY (`courseId`) REFERENCES `courses` (`id`),
  CONSTRAINT `courseware_organizationid_foreign` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`),
  CONSTRAINT `courseware_parentid_fk` FOREIGN KEY (`parentId`) REFERENCES `courseware` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `coursewareTags`
--

DROP TABLE IF EXISTS `coursewareTags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coursewareTags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tag` varchar(50) NOT NULL,
  `coursewareId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `coursewaretags_coursewareid_fk` (`coursewareId`),
  CONSTRAINT `coursewaretags_coursewareid_fk` FOREIGN KEY (`coursewareId`) REFERENCES `courseware` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `customReportCategories`
--

DROP TABLE IF EXISTS `customReportCategories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customReportCategories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `customreportcategories_organizationid_fk` (`organizationId`),
  CONSTRAINT `customreportcategories_organizationid_fk` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `customReportRecipients`
--

DROP TABLE IF EXISTS `customReportRecipients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customReportRecipients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(10) unsigned NOT NULL,
  `customReportId` int(10) unsigned NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  `dateCreated` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `customreportrecipients_userid_fk` (`userId`),
  KEY `customreportrecipients_customreportid_fk` (`customReportId`),
  KEY `customreportrecipients_organizationid_fk` (`organizationId`),
  CONSTRAINT `customreportrecipients_customreportid_fk` FOREIGN KEY (`customReportId`) REFERENCES `customReports` (`id`),
  CONSTRAINT `customreportrecipients_organizationid_fk` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`),
  CONSTRAINT `customreportrecipients_userid_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `customReports`
--

DROP TABLE IF EXISTS `customReports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customReports` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `query` varchar(2000) NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  `dateCreated` datetime NOT NULL,
  `name` varchar(250) NOT NULL,
  `categoryId` int(10) unsigned NOT NULL,
  `repeatMode` varchar(250) DEFAULT NULL,
  `outputFormat` varchar(250) DEFAULT NULL,
  `lastSent` datetime DEFAULT NULL,
  `lastFileName` varchar(2048) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `customreports_organizationid_fk` (`organizationId`),
  KEY `customreports_categoryid_fk` (`categoryId`),
  CONSTRAINT `customreports_categoryid_fk` FOREIGN KEY (`categoryId`) REFERENCES `customReportCategories` (`id`),
  CONSTRAINT `customreports_organizationid_fk` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `customRoles`
--

DROP TABLE IF EXISTS `customRoles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customRoles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `link` varchar(2048) NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  `description` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `customroles_organizationid_fk` (`organizationId`),
  CONSTRAINT `customroles_organizationid_fk` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `faq`
--

DROP TABLE IF EXISTS `faq`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `faq` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question` varchar(2000) NOT NULL,
  `answer` text,
  `accessCounter` int(10) unsigned NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  `isFeatured` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `faq_organizationid_fk` (`organizationId`),
  CONSTRAINT `faq_organizationid_fk` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `frequentFlyerRecords`
--

DROP TABLE IF EXISTS `frequentFlyerRecords`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `frequentFlyerRecords` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `airline` varchar(250) NOT NULL,
  `ffCode` varchar(250) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `userId` int(10) unsigned NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `frequentflyerrecords_userid_fk` (`userId`),
  KEY `frequentflyerrecords_organizationid_fk` (`organizationId`),
  CONSTRAINT `frequentflyerrecords_organizationid_fk` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`),
  CONSTRAINT `frequentflyerrecords_userid_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `helpItems`
--

DROP TABLE IF EXISTS `helpItems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `helpItems` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `path` varchar(250) NOT NULL,
  `title` varchar(250) NOT NULL,
  `content` text,
  `organizationId` int(10) unsigned NOT NULL,
  `dateCreated` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `helpitems_organizationid_fk` (`organizationId`),
  CONSTRAINT `helpitems_organizationid_fk` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `imageTags`
--

DROP TABLE IF EXISTS `imageTags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `imageTags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tag` varchar(50) NOT NULL,
  `courseImageId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `imagestags_courseimageid_fk` (`courseImageId`),
  CONSTRAINT `imagestags_courseimageid_fk` FOREIGN KEY (`courseImageId`) REFERENCES `courseImages` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `instructorCapabilities`
--

DROP TABLE IF EXISTS `instructorCapabilities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `instructorCapabilities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `courseId` int(10) unsigned NOT NULL,
  `userId` int(10) unsigned NOT NULL,
  `rating` tinyint(3) unsigned NOT NULL,
  `dateCreated` datetime NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `instructorcapabilities_courseid_fk` (`courseId`),
  KEY `instructorcapaiblities_userid_fk` (`userId`),
  CONSTRAINT `instructorcapabilities_courseid_fk` FOREIGN KEY (`courseId`) REFERENCES `courses` (`id`),
  CONSTRAINT `instructorcapaiblities_userid_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `instructorNotes`
--

DROP TABLE IF EXISTS `instructorNotes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `instructorNotes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content` varchar(2000) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `userId` int(10) unsigned NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `instructornotes_userid_fk` (`userId`),
  KEY `instructornotes_organizationid_fk` (`organizationId`),
  CONSTRAINT `instructornotes_organizationid_fk` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`),
  CONSTRAINT `instructornotes_userid_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `instructorTags`
--

DROP TABLE IF EXISTS `instructorTags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `instructorTags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tag` varchar(50) NOT NULL,
  `userId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `instructortags_userid_fk` (`userId`),
  CONSTRAINT `instructortags_userid_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `labCheckpoints`
--

DROP TABLE IF EXISTS `labCheckpoints`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `labCheckpoints` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tag` varchar(250) NOT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `labId` int(10) unsigned NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  `dateCreated` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `labcheckpoints_organizationid_fk` (`organizationId`),
  CONSTRAINT `labcheckpoints_organizationid_fk` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `labInstructions`
--

DROP TABLE IF EXISTS `labInstructions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `labInstructions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content` text,
  `dateCreated` datetime NOT NULL,
  `labId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `labinstructions_labid_fk` (`labId`),
  CONSTRAINT `labinstructions_labid_fk` FOREIGN KEY (`labId`) REFERENCES `labs` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `labTags`
--

DROP TABLE IF EXISTS `labTags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `labTags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tag` varchar(50) NOT NULL,
  `labId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `labtags_labid_fk` (`labId`),
  CONSTRAINT `labtags_labid_fk` FOREIGN KEY (`labId`) REFERENCES `labs` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `labs`
--

DROP TABLE IF EXISTS `labs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `labs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `containerId` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `userId` int(10) unsigned NOT NULL,
  `containerName` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `courseId` int(10) unsigned DEFAULT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `organizationId` int(10) unsigned NOT NULL,
  `labManualId` int(10) unsigned DEFAULT NULL,
  `parentId` int(10) unsigned DEFAULT NULL,
  `isAuthoritative` tinyint(1) NOT NULL DEFAULT '0',
  `timer` smallint(5) unsigned DEFAULT NULL,
  `timeStarted` datetime DEFAULT NULL,
  `datastore` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `platform` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'cypherpath',
  PRIMARY KEY (`id`),
  KEY `labs_userid_foreign` (`userId`),
  KEY `labs_courseid_foreign` (`courseId`),
  KEY `labs_organizationid_foreign` (`organizationId`),
  KEY `labs_labmanualid_fk` (`labManualId`),
  KEY `labs_parentid_fk` (`parentId`),
  CONSTRAINT `labs_courseid_foreign` FOREIGN KEY (`courseId`) REFERENCES `courses` (`id`),
  CONSTRAINT `labs_labmanualid_fk` FOREIGN KEY (`labManualId`) REFERENCES `courseWebLinks` (`id`),
  CONSTRAINT `labs_organizationid_foreign` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`),
  CONSTRAINT `labs_parentid_fk` FOREIGN KEY (`parentId`) REFERENCES `labs` (`id`),
  CONSTRAINT `labs_userid_foreign` FOREIGN KEY (`userId`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `locations`
--

DROP TABLE IF EXISTS `locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `locations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `addressOne` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `addressTwo` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `region` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contactName` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contactEmail` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contactPhone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `roomNumber` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `locations_organizationid_foreign` (`organizationId`),
  CONSTRAINT `locations_organizationid_foreign` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manualItems`
--

DROP TABLE IF EXISTS `manualItems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manualItems` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `content` text,
  `section` varchar(50) DEFAULT NULL,
  `parentId` int(10) unsigned DEFAULT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  `dateCreated` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `manualitems_parentid_fk` (`parentId`),
  KEY `manualitems_organizationid_fk` (`organizationId`),
  CONSTRAINT `manualitems_organizationid_fk` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`),
  CONSTRAINT `manualitems_parentid_fk` FOREIGN KEY (`parentId`) REFERENCES `manualItems` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `moduleCommentTopics`
--

DROP TABLE IF EXISTS `moduleCommentTopics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moduleCommentTopics` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `moduleId` int(10) unsigned NOT NULL,
  `courseCollaboratorId` int(10) unsigned NOT NULL,
  `isClosed` tinyint(1) NOT NULL DEFAULT '0',
  `isRemoved` tinyint(1) NOT NULL DEFAULT '0',
  `xPos` tinyint(3) unsigned NOT NULL,
  `yPos` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `modulecommenttopics_moduleid_fk` (`moduleId`),
  KEY `modulecommenttopics_coursecollaboratorid_fk` (`courseCollaboratorId`),
  CONSTRAINT `modulecommenttopics_coursecollaboratorid_fk` FOREIGN KEY (`courseCollaboratorId`) REFERENCES `courseCollaborators` (`id`),
  CONSTRAINT `modulecommenttopics_moduleid_fk` FOREIGN KEY (`moduleId`) REFERENCES `modules` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `moduleComments`
--

DROP TABLE IF EXISTS `moduleComments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moduleComments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content` varchar(1000) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `courseCollaboratorId` int(10) unsigned NOT NULL,
  `moduleCommentTopicId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `modulecomments_coursecollaboratorid_fk` (`courseCollaboratorId`),
  KEY `modulecomments_modulecommenttopicid_fk` (`moduleCommentTopicId`),
  CONSTRAINT `modulecomments_coursecollaboratorid_fk` FOREIGN KEY (`courseCollaboratorId`) REFERENCES `courseCollaborators` (`id`),
  CONSTRAINT `modulecomments_modulecommenttopicid_fk` FOREIGN KEY (`moduleCommentTopicId`) REFERENCES `moduleCommentTopics` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `moduleLayouts`
--

DROP TABLE IF EXISTS `moduleLayouts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moduleLayouts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `layout` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `moduleId` int(10) unsigned NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `modulelayouts_moduleid_foreign` (`moduleId`),
  KEY `modulelayouts_organizationid_foreign` (`organizationId`),
  CONSTRAINT `modulelayouts_moduleid_foreign` FOREIGN KEY (`moduleId`) REFERENCES `modules` (`id`),
  CONSTRAINT `modulelayouts_organizationid_foreign` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `moduleSectionCommentTopics`
--

DROP TABLE IF EXISTS `moduleSectionCommentTopics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moduleSectionCommentTopics` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `moduleSectionId` int(10) unsigned NOT NULL,
  `courseCollaboratorId` int(10) unsigned NOT NULL,
  `isClosed` tinyint(1) NOT NULL DEFAULT '0',
  `isRemoved` tinyint(1) NOT NULL DEFAULT '0',
  `xPos` tinyint(3) unsigned NOT NULL,
  `yPos` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `modulesectioncommenttopics_modulesectionid_fk` (`moduleSectionId`),
  KEY `modulesectioncommenttopics_coursecollaboratorid_fk` (`courseCollaboratorId`),
  CONSTRAINT `modulesectioncommenttopics_coursecollaboratorid_fk` FOREIGN KEY (`courseCollaboratorId`) REFERENCES `courseCollaborators` (`id`),
  CONSTRAINT `modulesectioncommenttopics_modulesectionid_fk` FOREIGN KEY (`moduleSectionId`) REFERENCES `moduleSections` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `moduleSectionComments`
--

DROP TABLE IF EXISTS `moduleSectionComments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moduleSectionComments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content` varchar(1000) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `courseCollaboratorId` int(10) unsigned NOT NULL,
  `moduleSectionCommentTopicId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `modulesectioncomments_coursecollaboratorid_fk` (`courseCollaboratorId`),
  KEY `modulesectioncomments_modulesectioncommenttopicid_fk` (`moduleSectionCommentTopicId`),
  CONSTRAINT `modulesectioncomments_coursecollaboratorid_fk` FOREIGN KEY (`courseCollaboratorId`) REFERENCES `courseCollaborators` (`id`),
  CONSTRAINT `modulesectioncomments_modulesectioncommenttopicid_fk` FOREIGN KEY (`moduleSectionCommentTopicId`) REFERENCES `moduleSectionCommentTopics` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `moduleSectionLayouts`
--

DROP TABLE IF EXISTS `moduleSectionLayouts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moduleSectionLayouts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `layout` longtext NOT NULL,
  `moduleSectionId` int(10) unsigned NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `modulesectionlayouts_modulesectionid_fk` (`moduleSectionId`),
  KEY `modulesectionlayouts_organizationId_fk` (`organizationId`),
  CONSTRAINT `modulesectionlayouts_modulesectionid_fk` FOREIGN KEY (`moduleSectionId`) REFERENCES `moduleSections` (`id`),
  CONSTRAINT `modulesectionlayouts_organizationId_fk` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `moduleSections`
--

DROP TABLE IF EXISTS `moduleSections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moduleSections` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `sortOrder` tinyint(3) unsigned NOT NULL,
  `moduleId` int(10) unsigned NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `modulesections_moduleid_fk` (`moduleId`),
  KEY `modulesections_organizationid_fk` (`organizationId`),
  CONSTRAINT `modulesections_moduleid_fk` FOREIGN KEY (`moduleId`) REFERENCES `modules` (`id`),
  CONSTRAINT `modulesections_organizationid_fk` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `modules`
--

DROP TABLE IF EXISTS `modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sortOrder` tinyint(3) unsigned NOT NULL,
  `dateCreated` datetime NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `courseId` int(10) unsigned NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `organizationId` int(10) unsigned NOT NULL,
  `weight` decimal(5,2) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `modules_courseid_foreign` (`courseId`),
  KEY `modules_organizationid_foreign` (`organizationId`),
  CONSTRAINT `modules_courseid_foreign` FOREIGN KEY (`courseId`) REFERENCES `courses` (`id`),
  CONSTRAINT `modules_organizationid_foreign` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `organizations`
--

DROP TABLE IF EXISTS `organizations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `organizations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(254) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contactPhone` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `addressOne` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `addressTwo` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `region` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `appId` char(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appSecret` char(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parentId` int(10) unsigned DEFAULT NULL,
  `skin` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'orange-black.css',
  `cpDomain` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cpUsername` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cpPassword` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cpClientId` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cpClientSecret` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `requirements` text COLLATE utf8mb4_unicode_ci,
  `structure` text COLLATE utf8mb4_unicode_ci,
  `instructorActive` tinyint(1) NOT NULL DEFAULT '1',
  `prereqsActive` tinyint(1) NOT NULL DEFAULT '1',
  `descriptionActive` tinyint(1) NOT NULL DEFAULT '1',
  `certificationActive` tinyint(1) NOT NULL DEFAULT '1',
  `coursewareActive` tinyint(1) NOT NULL DEFAULT '1',
  `requirementsActive` tinyint(1) NOT NULL DEFAULT '1',
  `structureActive` tinyint(1) NOT NULL DEFAULT '1',
  `learningOutcomes` text COLLATE utf8mb4_unicode_ci,
  `roadsToSuccess` text COLLATE utf8mb4_unicode_ci,
  `learningOutcomesActive` tinyint(1) NOT NULL DEFAULT '1',
  `roadsToSuccessActive` tinyint(1) NOT NULL DEFAULT '1',
  `outlineActive` tinyint(1) NOT NULL DEFAULT '1',
  `lateWork` text COLLATE utf8mb4_unicode_ci,
  `lateWorkActive` tinyint(1) NOT NULL DEFAULT '1',
  `viewingGradesActive` tinyint(1) NOT NULL DEFAULT '1',
  `letterGradesActive` tinyint(1) NOT NULL DEFAULT '1',
  `viewingGrades` text COLLATE utf8mb4_unicode_ci,
  `letterGrades` text COLLATE utf8mb4_unicode_ci,
  `attendClass` text COLLATE utf8mb4_unicode_ci,
  `participate` text COLLATE utf8mb4_unicode_ci,
  `buildRapport` text COLLATE utf8mb4_unicode_ci,
  `completeAssignments` text COLLATE utf8mb4_unicode_ci,
  `droppingCourse` text COLLATE utf8mb4_unicode_ci,
  `incompletePolicy` text COLLATE utf8mb4_unicode_ci,
  `accommodations` text COLLATE utf8mb4_unicode_ci,
  `integrity` text COLLATE utf8mb4_unicode_ci,
  `academicDishonesty` text COLLATE utf8mb4_unicode_ci,
  `attendClassActive` tinyint(1) NOT NULL DEFAULT '1',
  `participateActive` tinyint(1) NOT NULL DEFAULT '1',
  `buildRapportActive` tinyint(1) NOT NULL DEFAULT '1',
  `completeAssignmentsActive` tinyint(1) NOT NULL DEFAULT '1',
  `droppingCourseActive` tinyint(1) NOT NULL DEFAULT '1',
  `incompletePolicyActive` tinyint(1) NOT NULL DEFAULT '1',
  `accommodationsActive` tinyint(1) NOT NULL DEFAULT '1',
  `integrityActive` tinyint(1) NOT NULL DEFAULT '1',
  `academicDishonestyActive` tinyint(1) NOT NULL DEFAULT '1',
  `isTrainingEdition` tinyint(1) NOT NULL DEFAULT '0',
  `canVirtualize` tinyint(1) NOT NULL DEFAULT '0',
  `hostName` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `canCreateChildren` tinyint(1) NOT NULL DEFAULT '0',
  `touchnetUsername` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `touchnetPassword` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `touchnetUPayId` smallint(5) unsigned DEFAULT NULL,
  `touchnetTLinkUrl` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `touchnetUPayUrl` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `crmType` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `crmClientId` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `crmClientSecret` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enableCrm` tinyint(1) NOT NULL DEFAULT '0',
  `enableClientContactLogins` tinyint(1) NOT NULL DEFAULT '0',
  `enableLDAP` tinyint(1) NOT NULL DEFAULT '0',
  `ldapDomain` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ldapURL` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ldapDN` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ldapUsername` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ldapPassword` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ldapTokenAttribute` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `canManageLDAP` tinyint(1) NOT NULL DEFAULT '0',
  `enablePaymentProcessing` tinyint(1) NOT NULL DEFAULT '0',
  `enableVirtualization` tinyint(1) NOT NULL DEFAULT '0',
  `whiteLabel` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'EMF360',
  `studentRoleWhiteLabel` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Student',
  `supervisorRoleWhiteLabel` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Supervisor',
  `instructorRoleWhiteLabel` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Instructor',
  `contentDevRoleWhiteLabel` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Content Developer',
  `contentAdminRoleWhiteLabel` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Content Administrator',
  `systemAdminRoleWhiteLabel` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'System Administrator',
  `canDesignEvents` tinyint(1) NOT NULL DEFAULT '0',
  `enableSyllabus` tinyint(1) NOT NULL DEFAULT '1',
  `digitalCoursewareWhiteLabel` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Digital Courseware',
  `labManualWhiteLabel` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Lab Manual',
  `additionalReadingWhiteLabel` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Additional Reading',
  `softwareWhiteLabel` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Software',
  `sampleTestWhiteLabel` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Sample Test',
  `webLinkWhiteLabel` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Web Link',
  `otherWhiteLabel` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Other',
  `ldapIdentifierAttribute` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ldapObjectType` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `organizations_parentid_foreign` (`parentId`),
  CONSTRAINT `organizations_parentid_foreign` FOREIGN KEY (`parentId`) REFERENCES `organizations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `paAssessmentCourses`
--

DROP TABLE IF EXISTS `paAssessmentCourses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paAssessmentCourses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `courseId` int(10) unsigned NOT NULL,
  `paAssessmentId` int(10) unsigned NOT NULL,
  `dateCreated` datetime NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `paassessmentcourses_courseid_fk` (`courseId`),
  KEY `paassessmentcourses_paassessmentid_fk` (`paAssessmentId`),
  KEY `paassessmentcourses_organizationid_fk` (`organizationId`),
  CONSTRAINT `paassessmentcourses_courseid_fk` FOREIGN KEY (`courseId`) REFERENCES `courses` (`id`),
  CONSTRAINT `paassessmentcourses_organizationid_fk` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`),
  CONSTRAINT `paassessmentcourses_paassessmentid_fk` FOREIGN KEY (`paAssessmentId`) REFERENCES `paAssessments` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `paAssessments`
--

DROP TABLE IF EXISTS `paAssessments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paAssessments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `timeLimit` smallint(5) unsigned DEFAULT NULL,
  `assessmentType` varchar(25) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `labId` int(10) unsigned DEFAULT NULL,
  `courseExamId` int(10) unsigned DEFAULT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `paassessments_labid_fk` (`labId`),
  KEY `paassessments_courseexamid_fk` (`courseExamId`),
  KEY `paassessments_organizationid_fk` (`organizationId`),
  CONSTRAINT `paassessments_courseexamid_fk` FOREIGN KEY (`courseExamId`) REFERENCES `courseExams` (`id`),
  CONSTRAINT `paassessments_labid_fk` FOREIGN KEY (`labId`) REFERENCES `labs` (`id`),
  CONSTRAINT `paassessments_organizationid_fk` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `paBaselineUserAssessments`
--

DROP TABLE IF EXISTS `paBaselineUserAssessments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paBaselineUserAssessments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `paRoleBaselineUserId` int(10) unsigned NOT NULL,
  `paAssessmentId` int(10) unsigned NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateCompleted` datetime DEFAULT NULL,
  `score` decimal(5,2) unsigned DEFAULT NULL,
  `labId` int(10) unsigned DEFAULT NULL,
  `totalQuestions` smallint(5) unsigned DEFAULT NULL,
  `totalCorrect` smallint(5) unsigned DEFAULT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `organizationId` int(10) unsigned NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `timeTaken` time DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pabaselineuserassessments_parolebaselineuserid_fk` (`paRoleBaselineUserId`),
  KEY `pabaselineuserassessments_paassessmentid_fk` (`paAssessmentId`),
  KEY `pabaselineuserassessments_labid_fk` (`labId`),
  KEY `pabaselineuserassessments_organizationid_fk` (`organizationId`),
  CONSTRAINT `pabaselineuserassessments_labid_fk` FOREIGN KEY (`labId`) REFERENCES `labs` (`id`),
  CONSTRAINT `pabaselineuserassessments_organizationid_fk` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`),
  CONSTRAINT `pabaselineuserassessments_paassessmentid_fk` FOREIGN KEY (`paAssessmentId`) REFERENCES `paAssessments` (`id`),
  CONSTRAINT `pabaselineuserassessments_parolebaselineuserid_fk` FOREIGN KEY (`paRoleBaselineUserId`) REFERENCES `paRoleBaselineUsers` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `paBaselineUserExamAnswers`
--

DROP TABLE IF EXISTS `paBaselineUserExamAnswers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paBaselineUserExamAnswers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `paBaselineUserAssessmentId` int(10) unsigned NOT NULL,
  `courseExamId` int(10) unsigned NOT NULL,
  `courseExamQuestionId` int(10) unsigned NOT NULL,
  `courseExamAnswerId` int(10) unsigned NOT NULL,
  `dateCreated` datetime NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pabaselineuserexamanswers_pabaselineuserassessmentid_fk` (`paBaselineUserAssessmentId`),
  KEY `pabaselineuserexamanswers_courseexamid_fk` (`courseExamId`),
  KEY `pabaselineuserexamanswers_courseexamquestionid_fk` (`courseExamQuestionId`),
  KEY `pabaselineuserexamanswers_courseexamanswerid_fk` (`courseExamAnswerId`),
  KEY `pabaselineuserexamanswers_organizationid_fk` (`organizationId`),
  CONSTRAINT `pabaselineuserexamanswers_courseexamanswerid_fk` FOREIGN KEY (`courseExamAnswerId`) REFERENCES `courseExamAnswers` (`id`),
  CONSTRAINT `pabaselineuserexamanswers_courseexamid_fk` FOREIGN KEY (`courseExamId`) REFERENCES `courseExams` (`id`),
  CONSTRAINT `pabaselineuserexamanswers_courseexamquestionid_fk` FOREIGN KEY (`courseExamQuestionId`) REFERENCES `courseExamQuestions` (`id`),
  CONSTRAINT `pabaselineuserexamanswers_organizationid_fk` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`),
  CONSTRAINT `pabaselineuserexamanswers_pabaselineuserassessmentid_fk` FOREIGN KEY (`paBaselineUserAssessmentId`) REFERENCES `paBaselineUserAssessments` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `paCandidateAssessments`
--

DROP TABLE IF EXISTS `paCandidateAssessments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paCandidateAssessments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `paRoleCandidateId` int(10) unsigned NOT NULL,
  `paAssessmentId` int(10) unsigned NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateCompleted` datetime DEFAULT NULL,
  `score` decimal(5,2) unsigned DEFAULT NULL,
  `labId` int(10) unsigned DEFAULT NULL,
  `totalQuestions` smallint(5) unsigned DEFAULT NULL,
  `totalCorrect` smallint(5) unsigned DEFAULT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `organizationId` int(10) unsigned NOT NULL,
  `timeTaken` time DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pacandidateassessments_parolecandidateid_fk` (`paRoleCandidateId`),
  KEY `pacandidateassessments_passessmentid_fk` (`paAssessmentId`),
  KEY `pacandidateassessments_labid_fk` (`labId`),
  KEY `pacandidateassessments_organizationid_fk` (`organizationId`),
  CONSTRAINT `pacandidateassessments_labid_fk` FOREIGN KEY (`labId`) REFERENCES `labs` (`id`),
  CONSTRAINT `pacandidateassessments_organizationid_fk` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`),
  CONSTRAINT `pacandidateassessments_parolecandidateid_fk` FOREIGN KEY (`paRoleCandidateId`) REFERENCES `paRoleCandidates` (`id`),
  CONSTRAINT `pacandidateassessments_passessmentid_fk` FOREIGN KEY (`paAssessmentId`) REFERENCES `paAssessments` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `paCandidateExamAnswers`
--

DROP TABLE IF EXISTS `paCandidateExamAnswers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paCandidateExamAnswers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `paCandidateAssessmentId` int(10) unsigned NOT NULL,
  `courseExamId` int(10) unsigned NOT NULL,
  `courseExamQuestionId` int(10) unsigned NOT NULL,
  `courseExamAnswerId` int(10) unsigned NOT NULL,
  `dateCreated` datetime NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pacandidateexamanswers_pacandidateassessmentid_fk` (`paCandidateAssessmentId`),
  KEY `pacandidateexamanswers_courseexamid_fk` (`courseExamId`),
  KEY `pacandidateexamanswers_courseexamquestionid_fk` (`courseExamQuestionId`),
  KEY `pacandidateexamanswers_courseexamanswerid_fk` (`courseExamAnswerId`),
  KEY `pacandidateexamanswers_organizationid_fk` (`organizationId`),
  CONSTRAINT `pacandidateexamanswers_courseexamanswerid_fk` FOREIGN KEY (`courseExamAnswerId`) REFERENCES `courseExamAnswers` (`id`),
  CONSTRAINT `pacandidateexamanswers_courseexamid_fk` FOREIGN KEY (`courseExamId`) REFERENCES `courseExams` (`id`),
  CONSTRAINT `pacandidateexamanswers_courseexamquestionid_fk` FOREIGN KEY (`courseExamQuestionId`) REFERENCES `courseExamQuestions` (`id`),
  CONSTRAINT `pacandidateexamanswers_organizationid_fk` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`),
  CONSTRAINT `pacandidateexamanswers_pacandidateassessmentid_fk` FOREIGN KEY (`paCandidateAssessmentId`) REFERENCES `paCandidateAssessments` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `paCategories`
--

DROP TABLE IF EXISTS `paCategories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paCategories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `pacategories_organizationid_fk` (`organizationId`),
  CONSTRAINT `pacategories_organizationid_fk` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `paDomains`
--

DROP TABLE IF EXISTS `paDomains`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paDomains` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `padomains_organizationid_fk` (`organizationId`),
  CONSTRAINT `padomains_organizationid_fk` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `paMetricAssessments`
--

DROP TABLE IF EXISTS `paMetricAssessments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paMetricAssessments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `paMetricId` int(10) unsigned NOT NULL,
  `paAssessmentId` int(10) unsigned NOT NULL,
  `dateCreated` datetime NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `pametricassessments_pametricid_fk` (`paMetricId`),
  KEY `pametricassessments_passessmentid_fk` (`paAssessmentId`),
  KEY `pametricassessments_organizationid_fk` (`organizationId`),
  CONSTRAINT `pametricassessments_organizationid_fk` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`),
  CONSTRAINT `pametricassessments_pametricid_fk` FOREIGN KEY (`paMetricId`) REFERENCES `paMetrics` (`id`),
  CONSTRAINT `pametricassessments_passessmentid_fk` FOREIGN KEY (`paAssessmentId`) REFERENCES `paAssessments` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `paMetrics`
--

DROP TABLE IF EXISTS `paMetrics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paMetrics` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `paCategoryId` int(10) unsigned DEFAULT NULL,
  `paDomainId` int(10) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `pametrics_pacategoryid_fk` (`paCategoryId`),
  KEY `pametrics_padomainid_fk` (`paDomainId`),
  KEY `pametrics_organizationid_fk` (`organizationId`),
  CONSTRAINT `pametrics_organizationid_fk` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`),
  CONSTRAINT `pametrics_pacategoryid_fk` FOREIGN KEY (`paCategoryId`) REFERENCES `paCategories` (`id`),
  CONSTRAINT `pametrics_padomainid_fk` FOREIGN KEY (`paDomainId`) REFERENCES `paDomains` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `paRoleBaselineUsers`
--

DROP TABLE IF EXISTS `paRoleBaselineUsers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paRoleBaselineUsers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(10) unsigned NOT NULL,
  `paRoleId` int(10) unsigned NOT NULL,
  `dateCreated` datetime NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `parolebaselineusers_userid_fk` (`userId`),
  KEY `parolebaselineusers_paroleid_fk` (`paRoleId`),
  KEY `parolebaselineusers_organizationid_fk` (`organizationId`),
  CONSTRAINT `parolebaselineusers_organizationid_fk` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`),
  CONSTRAINT `parolebaselineusers_paroleid_fk` FOREIGN KEY (`paRoleId`) REFERENCES `paRoles` (`id`),
  CONSTRAINT `parolebaselineusers_userid_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `paRoleCandidates`
--

DROP TABLE IF EXISTS `paRoleCandidates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paRoleCandidates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(10) unsigned NOT NULL,
  `paRoleId` int(10) unsigned NOT NULL,
  `dateCreated` datetime NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `parolecandidates_userid_fk` (`userId`),
  KEY `parolecandidates_paroleid_fk` (`paRoleId`),
  KEY `parolecandidates_organizationid_fk` (`organizationId`),
  CONSTRAINT `parolecandidates_organizationid_fk` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`),
  CONSTRAINT `parolecandidates_paroleid_fk` FOREIGN KEY (`paRoleId`) REFERENCES `paRoles` (`id`),
  CONSTRAINT `parolecandidates_userid_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `paRoleMetrics`
--

DROP TABLE IF EXISTS `paRoleMetrics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paRoleMetrics` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `paRoleId` int(10) unsigned NOT NULL,
  `paMetricId` int(10) unsigned NOT NULL,
  `dateCreated` datetime NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `parolemetrics_pametricid_fk` (`paMetricId`),
  KEY `parolemetrics_paroleid_fk` (`paRoleId`),
  KEY `parolemetrics_organizationid_fk` (`organizationId`),
  CONSTRAINT `parolemetrics_organizationid_fk` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`),
  CONSTRAINT `parolemetrics_pametricid_fk` FOREIGN KEY (`paMetricId`) REFERENCES `paMetrics` (`id`),
  CONSTRAINT `parolemetrics_paroleid_fk` FOREIGN KEY (`paRoleId`) REFERENCES `paRoles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `paRoles`
--

DROP TABLE IF EXISTS `paRoles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paRoles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `paroles_organizationid_fk` (`organizationId`),
  CONSTRAINT `paroles_organizationid_fk` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pendingCourseEnrollments`
--

DROP TABLE IF EXISTS `pendingCourseEnrollments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pendingCourseEnrollments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `classId` int(10) unsigned NOT NULL,
  `userId` int(10) unsigned NOT NULL,
  `dateCreated` datetime NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  `billingFirstName` varchar(100) DEFAULT NULL,
  `billingLastName` varchar(100) DEFAULT NULL,
  `billingAddress` varchar(100) DEFAULT NULL,
  `billingCity` varchar(100) DEFAULT NULL,
  `billingState` varchar(100) DEFAULT NULL,
  `billingZip` varchar(100) DEFAULT NULL,
  `billingPhone` varchar(100) DEFAULT NULL,
  `billingEmail` varchar(100) DEFAULT NULL,
  `billingCost` decimal(10,2) DEFAULT NULL,
  `isTesting` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `pendingcourseenrollments_classid_fk` (`classId`),
  KEY `pendingcourseenrollments_userid_fk` (`userId`),
  KEY `pendingcourseenrollments_organizationid_fk` (`organizationId`),
  CONSTRAINT `pendingcourseenrollments_classid_fk` FOREIGN KEY (`classId`) REFERENCES `classes` (`id`),
  CONSTRAINT `pendingcourseenrollments_organizationid_fk` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`),
  CONSTRAINT `pendingcourseenrollments_userid_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `scormPackages`
--

DROP TABLE IF EXISTS `scormPackages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `scormPackages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `models` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `directory` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dateCreated` datetime NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `courseId` int(10) unsigned DEFAULT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  `parentId` int(10) unsigned DEFAULT NULL,
  `isAuthoritative` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `scormpackages_courseid_foreign` (`courseId`),
  KEY `scormpackages_organizationid_foreign` (`organizationId`),
  KEY `scormpackages_parentid_fk` (`parentId`),
  CONSTRAINT `scormpackages_courseid_foreign` FOREIGN KEY (`courseId`) REFERENCES `courses` (`id`),
  CONSTRAINT `scormpackages_organizationid_foreign` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`),
  CONSTRAINT `scormpackages_parentid_fk` FOREIGN KEY (`parentId`) REFERENCES `scormPackages` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `scormTags`
--

DROP TABLE IF EXISTS `scormTags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `scormTags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tag` varchar(50) NOT NULL,
  `scormPackageId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `scormtags_scormpackageid_fk` (`scormPackageId`),
  CONSTRAINT `scormtags_scormpackageid_fk` FOREIGN KEY (`scormPackageId`) REFERENCES `scormPackages` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stackTags`
--

DROP TABLE IF EXISTS `stackTags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stackTags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tag` varchar(50) NOT NULL,
  `stackId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `stacktags_stackid_fk` (`stackId`),
  CONSTRAINT `stacktags_stackid_fk` FOREIGN KEY (`stackId`) REFERENCES `stacks` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stacks`
--

DROP TABLE IF EXISTS `stacks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stacks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `description` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `courseId` int(10) unsigned DEFAULT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  `parentId` int(10) unsigned DEFAULT NULL,
  `isAuthoritative` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `stacks_courseid_foreign` (`courseId`),
  KEY `stacks_organizationid_foreign` (`organizationId`),
  KEY `stacks_parentid_fk` (`parentId`),
  CONSTRAINT `stacks_courseid_foreign` FOREIGN KEY (`courseId`) REFERENCES `courses` (`id`),
  CONSTRAINT `stacks_organizationid_foreign` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`),
  CONSTRAINT `stacks_parentid_fk` FOREIGN KEY (`parentId`) REFERENCES `stacks` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `studentExamAnswers`
--

DROP TABLE IF EXISTS `studentExamAnswers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `studentExamAnswers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `studentToClassId` int(10) unsigned NOT NULL,
  `courseExamId` int(10) unsigned NOT NULL,
  `courseExamQuestionId` int(10) unsigned NOT NULL,
  `courseExamAnswerId` int(10) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  `assignmentId` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `studentexamanswers_studenttoclassid_fk` (`studentToClassId`),
  KEY `studentexamanswers_courseexamid_fk` (`courseExamId`),
  KEY `studentexamanswers_courseexamquestionid_fk` (`courseExamQuestionId`),
  KEY `studentexamanswers_courseexamanswerid_fk` (`courseExamAnswerId`),
  KEY `studentexamanswers_organizationid_fk` (`organizationId`),
  KEY `studentexamanswers_assignmentid_fk` (`assignmentId`),
  CONSTRAINT `studentexamanswers_assignmentid_fk` FOREIGN KEY (`assignmentId`) REFERENCES `assignments` (`id`),
  CONSTRAINT `studentexamanswers_courseexamanswerid_fk` FOREIGN KEY (`courseExamAnswerId`) REFERENCES `courseExamAnswers` (`id`),
  CONSTRAINT `studentexamanswers_courseexamid_fk` FOREIGN KEY (`courseExamId`) REFERENCES `courseExams` (`id`),
  CONSTRAINT `studentexamanswers_courseexamquestionid_fk` FOREIGN KEY (`courseExamQuestionId`) REFERENCES `courseExamQuestions` (`id`),
  CONSTRAINT `studentexamanswers_organizationid_fk` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`),
  CONSTRAINT `studentexamanswers_studenttoclassid_fk` FOREIGN KEY (`studentToClassId`) REFERENCES `studentsToClasses` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `studentRefunds`
--

DROP TABLE IF EXISTS `studentRefunds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `studentRefunds` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `amount` decimal(10,2) NOT NULL,
  `notes` varchar(2000) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `studentToClassId` int(10) unsigned NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `studentrefunds_studenttoclassid_fk` (`studentToClassId`),
  KEY `studentrefunds_organizationid_fk` (`organizationId`),
  CONSTRAINT `studentrefunds_organizationid_fk` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`),
  CONSTRAINT `studentrefunds_studenttoclassid_fk` FOREIGN KEY (`studentToClassId`) REFERENCES `studentsToClasses` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `studentsToAssignments`
--

DROP TABLE IF EXISTS `studentsToAssignments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `studentsToAssignments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `studentToClassId` int(10) unsigned NOT NULL,
  `assignmentId` int(10) unsigned NOT NULL,
  `dateCreated` datetime NOT NULL,
  `score` decimal(5,2) unsigned DEFAULT NULL,
  `dateSubmitted` datetime DEFAULT NULL,
  `scormModel` longtext,
  `organizationId` int(10) unsigned NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `timeTaken` time DEFAULT NULL,
  `totalQuestions` smallint(5) unsigned DEFAULT NULL,
  `totalCorrect` smallint(5) unsigned DEFAULT NULL,
  `feedback` text,
  `labId` int(10) unsigned DEFAULT NULL,
  `sourceLabId` int(10) unsigned DEFAULT NULL,
  `isLocked` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `studentstoassignments_studenttoclassid_fk` (`studentToClassId`),
  KEY `studentstoassignments_assignmentid_fk` (`assignmentId`),
  KEY `studentstoassignments_organizationid_fk` (`organizationId`),
  KEY `studentstoassignments_labid_fk` (`labId`),
  KEY `studentstoassignments_sourcelabid_fk` (`sourceLabId`),
  CONSTRAINT `studentstoassignments_assignmentid_fk` FOREIGN KEY (`assignmentId`) REFERENCES `assignments` (`id`),
  CONSTRAINT `studentstoassignments_labid_fk` FOREIGN KEY (`labId`) REFERENCES `labs` (`id`),
  CONSTRAINT `studentstoassignments_organizationid_fk` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`),
  CONSTRAINT `studentstoassignments_sourcelabid_fk` FOREIGN KEY (`sourceLabId`) REFERENCES `labs` (`id`),
  CONSTRAINT `studentstoassignments_studenttoclassid_fk` FOREIGN KEY (`studentToClassId`) REFERENCES `studentsToClasses` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `studentsToClasses`
--

DROP TABLE IF EXISTS `studentsToClasses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `studentsToClasses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `isTesting` tinyint(1) NOT NULL,
  `dateRegistered` date NOT NULL,
  `dateCompleted` date DEFAULT NULL,
  `score` decimal(5,2) unsigned DEFAULT NULL,
  `studentId` int(10) unsigned NOT NULL,
  `classId` int(10) unsigned NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `evalDate` datetime DEFAULT NULL,
  `knowledgeOfSubject` float unsigned DEFAULT NULL,
  `realWorldExperience` float unsigned DEFAULT NULL,
  `madeMaterialsEasy` float unsigned DEFAULT NULL,
  `presentationSkill` float unsigned DEFAULT NULL,
  `providedSkills` float unsigned DEFAULT NULL,
  `preparedToApply` float unsigned DEFAULT NULL,
  `instructorComment` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `metExpectations` float unsigned DEFAULT NULL,
  `preparedToPassCert` float unsigned DEFAULT NULL,
  `challenged` float unsigned DEFAULT NULL,
  `satisfaction` float unsigned DEFAULT NULL,
  `wouldAttendOtherClass` float unsigned DEFAULT NULL,
  `experienceComment` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coursewareClear` float unsigned DEFAULT NULL,
  `coursewareRelevant` float unsigned DEFAULT NULL,
  `coursewareReinforced` float unsigned DEFAULT NULL,
  `coursewareComment` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `likedBestComment` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `likedLeastComment` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `additionalComment` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `evalSubmitted` tinyint(1) NOT NULL DEFAULT '0',
  `weightedScore` decimal(5,2) unsigned DEFAULT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `finalExamScore` decimal(5,2) unsigned DEFAULT NULL,
  `readyForFinalExam` tinyint(1) NOT NULL DEFAULT '0',
  `totalQuestions` smallint(5) unsigned DEFAULT NULL,
  `totalCorrect` smallint(5) unsigned DEFAULT NULL,
  `timeTaken` time DEFAULT NULL,
  `finalExamFeedback` text COLLATE utf8mb4_unicode_ci,
  `seatPaid` tinyint(1) NOT NULL DEFAULT '0',
  `billingFirstName` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billingLastName` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billingAddress` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billingCity` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billingState` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billingZip` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billingPhone` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billingEmail` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billingCost` decimal(10,2) DEFAULT NULL,
  `transactionId` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receiptNumber` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `invoiceNumber` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paymentSession` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trainingPlanUserId` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `studentstoclasses_studentid_foreign` (`studentId`),
  KEY `studentstoclasses_classid_foreign` (`classId`),
  KEY `studentstoclasses_organizationid_foreign` (`organizationId`),
  KEY `studentstoclasses_trainingplanuserid_fk` (`trainingPlanUserId`),
  CONSTRAINT `studentstoclasses_classid_foreign` FOREIGN KEY (`classId`) REFERENCES `classes` (`id`),
  CONSTRAINT `studentstoclasses_organizationid_foreign` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`),
  CONSTRAINT `studentstoclasses_studentid_foreign` FOREIGN KEY (`studentId`) REFERENCES `users` (`id`),
  CONSTRAINT `studentstoclasses_trainingplanuserid_fk` FOREIGN KEY (`trainingPlanUserId`) REFERENCES `trainingPlanUsers` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `studentsToModules`
--

DROP TABLE IF EXISTS `studentsToModules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `studentsToModules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `studentToClassId` int(10) unsigned NOT NULL,
  `moduleId` int(10) unsigned NOT NULL,
  `dateCreated` datetime NOT NULL,
  `score` decimal(5,2) unsigned DEFAULT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `organizationId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `studentstomodules_moduleid_fk` (`moduleId`),
  KEY `studentstomodules_studentoclassid_fk` (`studentToClassId`),
  KEY `studentstomodules_organizationid_fk` (`organizationId`),
  CONSTRAINT `studentstomodules_moduleid_fk` FOREIGN KEY (`moduleId`) REFERENCES `modules` (`id`),
  CONSTRAINT `studentstomodules_organizationid_fk` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`),
  CONSTRAINT `studentstomodules_studentoclassid_fk` FOREIGN KEY (`studentToClassId`) REFERENCES `studentsToClasses` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `submittedDocs`
--

DROP TABLE IF EXISTS `submittedDocs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `submittedDocs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `link` varchar(2048) DEFAULT NULL,
  `studentToClassId` int(10) unsigned NOT NULL,
  `assignmentId` int(10) unsigned NOT NULL,
  `dateCreated` datetime NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  `fileSize` float unsigned DEFAULT NULL,
  `mime` varchar(100) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `submitteddocs_studentoclassid_fk` (`studentToClassId`),
  KEY `submitteddocs_assignmentid_fk` (`assignmentId`),
  KEY `submitteddocs_organizationid_fk` (`organizationId`),
  CONSTRAINT `submitteddocs_assignmentid_fk` FOREIGN KEY (`assignmentId`) REFERENCES `assignments` (`id`),
  CONSTRAINT `submitteddocs_organizationid_fk` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`),
  CONSTRAINT `submitteddocs_studentoclassid_fk` FOREIGN KEY (`studentToClassId`) REFERENCES `studentsToClasses` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `surveyAnswers`
--

DROP TABLE IF EXISTS `surveyAnswers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `surveyAnswers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `answerText` varchar(2000) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `surveyQuestionId` int(10) unsigned NOT NULL,
  `surveyId` int(10) unsigned NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `surveyanswers_surveyquestionid_fk` (`surveyQuestionId`),
  KEY `surveyanswers_surveyid_fk` (`surveyId`),
  KEY `surveyanswers_organizationid_fk` (`organizationId`),
  CONSTRAINT `surveyanswers_organizationid_fk` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`),
  CONSTRAINT `surveyanswers_surveyid_fk` FOREIGN KEY (`surveyId`) REFERENCES `surveys` (`id`),
  CONSTRAINT `surveyanswers_surveyquestionid_fk` FOREIGN KEY (`surveyQuestionId`) REFERENCES `surveyQuestions` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `surveyQuestionCategories`
--

DROP TABLE IF EXISTS `surveyQuestionCategories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `surveyQuestionCategories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `surveyId` int(10) unsigned NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `surveyquestioncategories_surveyid_fk` (`surveyId`),
  KEY `surveyquestioncategories_organizationid_fk` (`organizationId`),
  CONSTRAINT `surveyquestioncategories_organizationid_fk` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`),
  CONSTRAINT `surveyquestioncategories_surveyid_fk` FOREIGN KEY (`surveyId`) REFERENCES `surveys` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `surveyQuestions`
--

DROP TABLE IF EXISTS `surveyQuestions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `surveyQuestions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `questionText` varchar(2000) DEFAULT NULL,
  `type` varchar(100) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `surveyId` int(10) unsigned NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  `surveyQuestionCategoryId` int(10) unsigned NOT NULL,
  `isRequired` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `surveyquestions_surveyid_fk` (`surveyId`),
  KEY `surveyquestions_organizationid_fk` (`organizationId`),
  KEY `surveyquestions_surveyquestioncategoryid_fk` (`surveyQuestionCategoryId`),
  CONSTRAINT `surveyquestions_organizationid_fk` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`),
  CONSTRAINT `surveyquestions_surveyid_fk` FOREIGN KEY (`surveyId`) REFERENCES `surveys` (`id`),
  CONSTRAINT `surveyquestions_surveyquestioncategoryid_fk` FOREIGN KEY (`surveyQuestionCategoryId`) REFERENCES `surveyQuestionCategories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `surveyRespondents`
--

DROP TABLE IF EXISTS `surveyRespondents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `surveyRespondents` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(10) unsigned NOT NULL,
  `surveyId` int(10) unsigned NOT NULL,
  `isComplete` tinyint(1) NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateCompleted` datetime DEFAULT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `surveyrespondents_userid_fk` (`userId`),
  KEY `surveyrespondents_surveyid_fk` (`surveyId`),
  KEY `surveyrespondents_organizationid_fk` (`organizationId`),
  CONSTRAINT `surveyrespondents_organizationid_fk` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`),
  CONSTRAINT `surveyrespondents_surveyid_fk` FOREIGN KEY (`surveyId`) REFERENCES `surveys` (`id`),
  CONSTRAINT `surveyrespondents_userid_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `surveyResponses`
--

DROP TABLE IF EXISTS `surveyResponses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `surveyResponses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rating` float unsigned DEFAULT NULL,
  `shortAnswer` varchar(2000) DEFAULT NULL,
  `surveyAnswerId` int(10) unsigned DEFAULT NULL,
  `surveyQuestionId` int(10) unsigned NOT NULL,
  `surveyId` int(10) unsigned NOT NULL,
  `userId` int(10) unsigned NOT NULL,
  `studentToClassId` int(10) unsigned DEFAULT NULL,
  `trainingPlanCourseId` int(10) unsigned DEFAULT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  `dateCreated` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `surveyresponses_surveyanswerid_fk` (`surveyAnswerId`),
  KEY `surveyresponses_surveyquestionid_fk` (`surveyQuestionId`),
  KEY `surveyresponses_surveyid_fk` (`surveyId`),
  KEY `surveyresponses_userid_fk` (`userId`),
  KEY `surveyresponses_studentoclassid_fk` (`studentToClassId`),
  KEY `surveyresponses_trainingplancourseid_fk` (`trainingPlanCourseId`),
  KEY `surveyresponses_organizationid_fk` (`organizationId`),
  CONSTRAINT `surveyresponses_organizationid_fk` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`),
  CONSTRAINT `surveyresponses_studentoclassid_fk` FOREIGN KEY (`studentToClassId`) REFERENCES `studentsToClasses` (`id`),
  CONSTRAINT `surveyresponses_surveyanswerid_fk` FOREIGN KEY (`surveyAnswerId`) REFERENCES `surveyAnswers` (`id`),
  CONSTRAINT `surveyresponses_surveyid_fk` FOREIGN KEY (`surveyId`) REFERENCES `surveys` (`id`),
  CONSTRAINT `surveyresponses_surveyquestionid_fk` FOREIGN KEY (`surveyQuestionId`) REFERENCES `surveyQuestions` (`id`),
  CONSTRAINT `surveyresponses_trainingplancourseid_fk` FOREIGN KEY (`trainingPlanCourseId`) REFERENCES `trainingPlanCourses` (`id`),
  CONSTRAINT `surveyresponses_userid_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `surveys`
--

DROP TABLE IF EXISTS `surveys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `surveys` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `organizationId` int(10) unsigned NOT NULL,
  `type` varchar(50) NOT NULL DEFAULT 'standard',
  PRIMARY KEY (`id`),
  KEY `surveys_organizationid_fk` (`organizationId`),
  CONSTRAINT `surveys_organizationid_fk` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tasks`
--

DROP TABLE IF EXISTS `tasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tasks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dueDate` date DEFAULT NULL,
  `dueTime` time DEFAULT NULL,
  `reminderMode` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reminderDate` date DEFAULT NULL,
  `reminderTime` time DEFAULT NULL,
  `reminderDismissed` tinyint(1) NOT NULL DEFAULT '0',
  `isClosed` tinyint(1) NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `userId` int(10) unsigned NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tasks_userid_foreign` (`userId`),
  KEY `tasks_organizationid_foreign` (`organizationId`),
  CONSTRAINT `tasks_organizationid_foreign` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`),
  CONSTRAINT `tasks_userid_foreign` FOREIGN KEY (`userId`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `teamMembers`
--

DROP TABLE IF EXISTS `teamMembers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teamMembers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `teamId` int(10) unsigned NOT NULL,
  `userId` int(10) unsigned NOT NULL,
  `dateCreated` datetime NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `teammembers_teamid_fk` (`teamId`),
  KEY `teammembers_userid_fk` (`userId`),
  KEY `teammembers_organizationid_fk` (`organizationId`),
  CONSTRAINT `teammembers_organizationid_fk` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`),
  CONSTRAINT `teammembers_teamid_fk` FOREIGN KEY (`teamId`) REFERENCES `teams` (`id`),
  CONSTRAINT `teammembers_userid_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `teams`
--

DROP TABLE IF EXISTS `teams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teams` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `teams_organizationid_fk` (`organizationId`),
  CONSTRAINT `teams_organizationid_fk` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trainingEvents`
--

DROP TABLE IF EXISTS `trainingEvents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trainingEvents` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `url` varchar(2048) DEFAULT NULL,
  `startDate` date NOT NULL,
  `endDate` date NOT NULL,
  `image` varchar(2048) DEFAULT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `organizationId` int(10) unsigned NOT NULL,
  `eventKey` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `trainingevents_organizationid_fk` (`organizationId`),
  CONSTRAINT `trainingevents_organizationid_fk` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trainingPlanCourses`
--

DROP TABLE IF EXISTS `trainingPlanCourses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trainingPlanCourses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `trainingPlanId` int(10) unsigned NOT NULL,
  `courseId` int(10) unsigned DEFAULT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `sortOrder` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `classId` int(10) unsigned DEFAULT NULL,
  `isCourse` tinyint(1) NOT NULL DEFAULT '1',
  `trainingEventId` int(10) unsigned DEFAULT NULL,
  `surveyId` int(10) unsigned DEFAULT NULL,
  `type` varchar(50) NOT NULL DEFAULT 'course',
  PRIMARY KEY (`id`),
  KEY `trainingplancourses_trainingplanid_fk` (`trainingPlanId`),
  KEY `trainingplancourses_courseid_fk` (`courseId`),
  KEY `trainingplancourses_classid_fk` (`classId`),
  KEY `trainingplancourses_trainingeventid_fk` (`trainingEventId`),
  KEY `trainingplancourses_surveyid_fk` (`surveyId`),
  CONSTRAINT `trainingplancourses_classid_fk` FOREIGN KEY (`classId`) REFERENCES `classes` (`id`),
  CONSTRAINT `trainingplancourses_courseid_fk` FOREIGN KEY (`courseId`) REFERENCES `courses` (`id`),
  CONSTRAINT `trainingplancourses_surveyid_fk` FOREIGN KEY (`surveyId`) REFERENCES `surveys` (`id`),
  CONSTRAINT `trainingplancourses_trainingeventid_fk` FOREIGN KEY (`trainingEventId`) REFERENCES `trainingEvents` (`id`),
  CONSTRAINT `trainingplancourses_trainingplanid_fk` FOREIGN KEY (`trainingPlanId`) REFERENCES `trainingPlans` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trainingPlanUsers`
--

DROP TABLE IF EXISTS `trainingPlanUsers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trainingPlanUsers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `trainingPlanId` int(10) unsigned NOT NULL,
  `userId` int(10) unsigned NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `trainingplanusers_trainingplanid_fk` (`trainingPlanId`),
  KEY `trainingplanusers_userid_fk` (`userId`),
  CONSTRAINT `trainingplanusers_trainingplanid_fk` FOREIGN KEY (`trainingPlanId`) REFERENCES `trainingPlans` (`id`),
  CONSTRAINT `trainingplanusers_userid_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trainingPlans`
--

DROP TABLE IF EXISTS `trainingPlans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trainingPlans` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `logo` varchar(2048) DEFAULT NULL,
  `startDate` date NOT NULL,
  `endDate` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `trainingplans_organizationid_fk` (`organizationId`),
  CONSTRAINT `trainingplans_organizationid_fk` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userCertifications`
--

DROP TABLE IF EXISTS `userCertifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userCertifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dateIssued` date NOT NULL,
  `dateExpires` date DEFAULT NULL,
  `isActive` tinyint(1) NOT NULL,
  `pdf` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `certificationId` int(10) unsigned NOT NULL,
  `userId` int(10) unsigned NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `usercertifications_certificationid_foreign` (`certificationId`),
  KEY `usercertifications_userid_foreign` (`userId`),
  KEY `usercertifications_organizationid_foreign` (`organizationId`),
  CONSTRAINT `usercertifications_certificationid_foreign` FOREIGN KEY (`certificationId`) REFERENCES `certifications` (`id`),
  CONSTRAINT `usercertifications_organizationid_foreign` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`),
  CONSTRAINT `usercertifications_userid_foreign` FOREIGN KEY (`userId`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userCustomFields`
--

DROP TABLE IF EXISTS `userCustomFields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userCustomFields` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `content` varchar(250) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `userId` int(10) unsigned NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `usercustomfields_userid_fk` (`userId`),
  KEY `usercustomfields_organizationid_fk` (`organizationId`),
  CONSTRAINT `usercustomfields_organizationid_fk` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`),
  CONSTRAINT `usercustomfields_userid_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userCustomRoles`
--

DROP TABLE IF EXISTS `userCustomRoles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userCustomRoles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(10) unsigned NOT NULL,
  `customRoleId` int(10) unsigned NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `usercustomroles_customroleid_fk` (`customRoleId`),
  KEY `usercustomroles_userid_fk` (`userId`),
  CONSTRAINT `usercustomroles_customroleid_fk` FOREIGN KEY (`customRoleId`) REFERENCES `customRoles` (`id`),
  CONSTRAINT `usercustomroles_userid_fk` FOREIGN KEY (`userId`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstName` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `middleName` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastName` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nickname` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cypherpathId` int(10) unsigned DEFAULT NULL,
  `email` varchar(254) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `salt` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` char(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isFirstLogin` tinyint(1) NOT NULL DEFAULT '1',
  `cellPhone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `homePhone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `workPhone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fax` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `addressOne` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `addressTwo` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `region` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `pob` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isActive` tinyint(1) NOT NULL DEFAULT '1',
  `hasCard` tinyint(1) NOT NULL DEFAULT '0',
  `cardNumber` char(4) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hasPassport` tinyint(1) NOT NULL DEFAULT '0',
  `passportExpiration` date DEFAULT NULL,
  `photo` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bio` text COLLATE utf8mb4_unicode_ci,
  `resume` text COLLATE utf8mb4_unicode_ci,
  `resumeFile` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jobTitle` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `resetCode` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instructor` tinyint(1) NOT NULL DEFAULT '0',
  `organizationId` int(10) unsigned NOT NULL,
  `dateCreated` datetime NOT NULL,
  `timezone` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `supervisor` tinyint(1) NOT NULL DEFAULT '0',
  `student` tinyint(1) NOT NULL DEFAULT '0',
  `contentAdmin` tinyint(1) NOT NULL DEFAULT '0',
  `systemAdmin` tinyint(1) NOT NULL DEFAULT '0',
  `labCpu` tinyint(3) unsigned NOT NULL DEFAULT '50',
  `labRam` mediumint(8) unsigned NOT NULL DEFAULT '50000',
  `labStorage` mediumint(8) unsigned NOT NULL DEFAULT '512000',
  `iaDesignation` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `iaDuty` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `iaLevel` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rank` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subUnit` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billet` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contentDeveloper` tinyint(1) NOT NULL DEFAULT '0',
  `securityQuestionOne` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `securityAnswerOne` char(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `securityQuestionTwo` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `securityAnswerTwo` char(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `securityQuestionsChosen` tinyint(1) NOT NULL DEFAULT '0',
  `instructorDailyRate` decimal(10,2) unsigned DEFAULT NULL,
  `instructorPreferredAirport` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `clientContact` tinyint(1) NOT NULL DEFAULT '0',
  `crmId` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isCrmContact` tinyint(1) NOT NULL DEFAULT '0',
  `isLdapUser` tinyint(1) NOT NULL DEFAULT '0',
  `ldapDN` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ldapUsername` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `eventDesigner` tinyint(1) NOT NULL DEFAULT '0',
  `hasTakenRegistrationSurvey` tinyint(1) NOT NULL DEFAULT '0',
  `ssoId` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `users_organizationid_foreign` (`organizationId`),
  CONSTRAINT `users_organizationid_foreign` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vendors`
--

DROP TABLE IF EXISTS `vendors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vendors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `organizationId` int(10) unsigned NOT NULL,
  `isPublic` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `vendors_organizationid_foreign` (`organizationId`),
  CONSTRAINT `vendors_organizationid_foreign` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `videoTags`
--

DROP TABLE IF EXISTS `videoTags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `videoTags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tag` varchar(50) NOT NULL,
  `courseVideoId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `videotags_coursevideoid_fk` (`courseVideoId`),
  CONSTRAINT `videotags_coursevideoid_fk` FOREIGN KEY (`courseVideoId`) REFERENCES `courseVideos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

INSERT INTO `organizations` (`name`, `email`, `contactPhone`, `dateCreated`)
VALUES ('RSAF', 'admin@rsaf.com', '555-555-5555', 'NOW()');

INSERT INTO `users` (`firstName`, `lastName`, `email`, `username`, `salt`, `password`, `isFirstLogin`, `organizationId`, `dateCreated`, `supervisor`, `student`, `contentAdmin`, `systemAdmin`, `contentDeveloper`)
VALUES ('Admin', 'User', 'adminuser@rsaf.com', 'adminuser', '79005228fd5a47a6', '075a898d3891385c7448da6b966c48263eb28d70daca3fa650f1617ff0dde7b6', 0, 1, 'NOW()', 1, 1, 1, 1, 1);

/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-04-27 18:32:36
