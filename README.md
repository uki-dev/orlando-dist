# README #

Here are the steps to get your application up and running. This guide assumes
you have already installed Docker and Docker Compose on the target system. The 
containers are designed to run on single virtual machine.

### How do I get set up? ###

Start by cloning the project: **git clone https://bitbucket.org/uki-dev/orlando-dist.git**

* To bring up the project, execute **docker-compose up -d** within the orlando-dist directory.
* To bring down the project, execute **docker-compose down** within the orlando-dist directory.
* The web interface listens on 8080 by default. To modify the web port binding, change the port reference under the web service from 8080:8080 80:8080 in the docker-compose.yml file.
### Who do I talk to? ###

* Steven Redman (sredman@ultimateknowledge.com)